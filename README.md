# DiNeROS

Distributed Petri Nets to ROS Toolchain

Clone the full project via: 

```git clone --recurse-submodules git@git-st.inf.tu-dresden.de:jastadd/jastadd-ceti/dineros.git```

**Warning:** The pkg-generator is currently not working. Please extend the [example](https://git-st.inf.tu-dresden.de/dineros/framework/robotic-sorting), while the generator is updated.
